module shifter#(
				parameter	DATAIN_BIT = 6,
				parameter	DATAOUT_BIT = 7										
				)
(
input								clk,
input								rst_n,
input[2**DATAIN_BIT-1:0]			datain,
input								datain_en,
output[2**DATAOUT_BIT-1:0]			dataout,
output								dataout_en
);


localparam	MUX_LENGTH = DATAOUT_BIT - DATAIN_BIT;
localparam	DATAIN_WIDTH = 2**DATAIN_BIT;


reg [DATAIN_WIDTH - 1: 0] 				shift_reg [2**MUX_LENGTH-1:0];
reg 									shift_en;
reg [MUX_LENGTH - 1 : 0] 				shift_counter;


always@(posedge clk or negedge rst_n)
begin
	if(~rst_n)
		shift_counter <=  0;
	else if(datain_en)
		shift_counter <=  shift_counter + 1;
end


always@(posedge clk or negedge rst_n)
begin
	if(~rst_n)
		shift_en <=  0;
	else if((shift_counter == {(MUX_LENGTH){1'b1}}) && datain_en)
		shift_en <=  1;
	else
		shift_en <=  0;
end


genvar i;

generate
   for (i=0; i < 2**MUX_LENGTH;i=i+1) //DATAOUT_BIT - DATAIN_BIT + 1; i=i+1) 
   begin: u_cache_din
    assign  dataout[i*DATAIN_WIDTH+DATAIN_WIDTH-1 : i*DATAIN_WIDTH] = shift_reg[2**MUX_LENGTH-i-1];
   end
endgenerate


integer k;
always@(posedge clk or negedge rst_n)
begin
    if(~rst_n)
        for(k=1; k < 2**MUX_LENGTH ; k=k+1)
        shift_reg[k] <=  {DATAIN_WIDTH{1'b0}}; 
    else if (datain_en)
		for(k=1;  k < 2**MUX_LENGTH; k=k+1)
        shift_reg[k] <=  shift_reg[k-1];
end

always@(posedge clk or negedge rst_n)
begin
    if(~rst_n)
        shift_reg[0] <=  {DATAIN_WIDTH{1'b0}};  
	else if (datain_en)
        shift_reg[0] <=  datain;
end

assign		dataout_en = shift_en;

endmodule
