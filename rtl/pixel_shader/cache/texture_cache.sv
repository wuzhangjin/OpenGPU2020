//////////////////////////////////////////////////////////////////////////////////
// Company:   
// Engineer:  Tian Xiang
// 
// Create Date:    17:44:03 06/03/2015 
// Design Name: 
// Module Name:    texture_cache 
// Project Name:   AHMI Lattice
// Target Devices: Xilinx & Lattice
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 1.00 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "gpu_bus.sv"
module texture_cache #(
parameter   BUS_WIDTH = 128,
parameter   SYSTEM_ADDR_BITS = 32,
parameter   CACHE_LINE_BITS = 256,                 // CACHE_LINE SIZE                                         
parameter   CACHE_ADDR_BITS = 6,                   // each way of cache has  2**CACHE_ADDR_BITS of CACHE_LINE 
parameter   RAM_DATA_BITS = 8,                     // data width of cache data ram is 2**RAM_DATA_BITS bits   
parameter   CACHE_WAY_NUMBER_LOG2 = 1,             // cache way number is 2**CACHE_WAY_NUMBER_LOG2            
parameter   DATA_RD_FIFO_WIDTH = 7
)
(
    input              SCAN_mode,
    input              clk,
    input              arst_n,
    input              rec_busy,
    input              cache_busy_in,
    input              GPE_start,
    // texture fetch module interface
    input      [SYSTEM_ADDR_BITS-1:0]  block_addr0,
    input      [6:0]   in_block_addr0,
    input      [SYSTEM_ADDR_BITS-1:0]  block_addr1,
    input      [6:0]   in_block_addr1, 
    input      [SYSTEM_ADDR_BITS-1:0]  block_addr2,
    input      [6:0]   in_block_addr2, 
    input      [SYSTEM_ADDR_BITS-1:0]  block_addr3,
    input      [6:0]   in_block_addr3, 
    input      [3:0]   textype,
    input      [10:0]  texture_width,
    input              rotation_en,
    input              rd_en,
    input              filt_en,
    output     [31:0]  texel_32_0,
    output             texel_32_en,
    output     [31:0]  texel_32_1,
    output     [31:0]  texel_32_2,
    output     [31:0]  texel_32_3,
    output             cache_busy,
    //  (flash) memory interface
    gpu_bus            src[4]
);


localparam BUS_BURST_LENGTH = (1 << ($clog2(CACHE_LINE_BITS) - $clog2(BUS_WIDTH))) - 1;
localparam BUS_BIT = $clog2(BUS_WIDTH) ;
localparam ARB_BURST_LENGTH = 2**($clog2(CACHE_LINE_BITS)-RAM_DATA_BITS)-1;
localparam SYS_WIDTH = SYSTEM_ADDR_BITS;  
localparam CACHE_SIZE = $clog2(CACHE_LINE_BITS)+CACHE_ADDR_BITS+CACHE_WAY_NUMBER_LOG2;

wire   [5:0]                req_bl = ARB_BURST_LENGTH;
//wire                      cmd_fifo_empty;
wire[SYSTEM_ADDR_BITS-1:0]  l1_cache_addr; 
wire                        l1_cache_addr_valid;
wire                        l1_cache_burst_busy;


wire   [(2**RAM_DATA_BITS)-1:0]  cache_din;
wire          shift_en;   



// Decoder input interface
wire   [(2**RAM_DATA_BITS)-1:0]  l1_dout0;
wire          decoder_outen0;
wire  [23:0]  color0;
wire[7:0]          alpha0;
wire   [(2**RAM_DATA_BITS)-1:0]  l1_dout1;
wire          decoder_outen1;
wire  [23:0]  color1;
wire[7:0]          alpha1;
wire   [(2**RAM_DATA_BITS)-1:0]  l1_dout2;
wire          decoder_outen2;
wire  [23:0]  color2;
wire[7:0]          alpha2;
wire   [(2**RAM_DATA_BITS)-1:0]  l1_dout3;
wire          decoder_outen3;
wire  [23:0]  color3;
wire[7:0]          alpha3;


//reg           rd_en_ff1;

//reg   [31:0]  block_addr_ff;
//wire  [31:0]  block_addr_hold = rd_en ? block_addr : block_addr_ff;

//reg   [6:0]   in_block_addr_ff;
//wire  [6:0]   in_block_addr_hold = rd_en ? in_block_addr: in_block_addr_ff;

wire [1:0] block_addr_out0;
wire [6:0] in_block_addr_out0;
wire [1:0] block_addr_out1;
wire [6:0] in_block_addr_out1; 
wire [1:0] block_addr_out2;
wire [6:0] in_block_addr_out2; 
wire [1:0] block_addr_out3;
wire [6:0] in_block_addr_out3; 

wire    l1_out_en;

wire [(2+7)*4-1:0]          addr_fifo_inter_data;
wire                        inter_wr_rd;
wire                        addr_fifo_first_full;
wire                        addr_fifo_first_empty; 


wire    blockaddr_fifo_full;
wire    addr_fifo_full;

assign  texel_32_en = decoder_outen0;


assign  texel_32_0 = {alpha0,color0}; 
assign  texel_32_1 = {alpha1,color1};
assign  texel_32_2 = {alpha2,color2}; 
assign  texel_32_3 = {alpha3,color3}; 

//always @(posedge clk or negedge arst_n)
//begin
//    if(~arst_n)
//    begin
//        block_addr_ff <=  0;
//        in_block_addr_ff <=  0;
//    end
//    else if(rd_en & !cache_busy)
//    begin
//        block_addr_ff <=  block_addr;
//        in_block_addr_ff <=  in_block_addr;
//    end
//end

//always @(posedge clk or negedge arst_n)
//begin
//    if(~arst_n)
//    begin
//        rd_en_ff1 <=  0;
//    end
//    else if(!cache_busy)   rd_en_ff1 <=  rd_en;
//end



//cache_top#(
//        .SYS_WIDTH(`CACHE_ADDR_BITS),
//        .CACHE_ADDR_WIDTH(CACHE_ADDR_BITS),
//        .CACHE_DATA_WIDTH($clog2(CACHE_LINE_BITS)),
//        .RAM_DATA_WIDTH(RAM_DATA_BITS),
//        .CACHE_NUM_LOG2(CACHE_WAY_NUMBER_LOG2),
//        .DATA_RD_FIFO_WIDTH(DATA_RD_FIFO_WIDTH)
//            )
//    cache_top       
//(
//.clk                 (clk),
//.rst_n               (arst_n),
//.cache_rd_addr       (block_addr),
//.cache_rd_en         (rd_en),
//.force_en            (in_block_addr[6]),
//.cache_busy_in       (cache_busy_in),
//.cache_busy_out      (cache_busy),
//.cache_rd_data       (l1_dout),
//.cache_rd_data_vaild (l1_out_en),
////.cmd_fifo_empty      (cmd_fifo_empty),
//.bus_addr            (bus_addr),
//.bus_cmd             (bus_cmd),
//.bus_dout            (cache_din),
//.bus_den             (shift_en)
//);

wire[3:0]      force_en;
wire           cache_busy_out;
wire           rd_data_en;  

wire[SYS_WIDTH-1:0]               rd_addr_0;
wire                              rd_cmd_0;
wire[CACHE_LINE_BITS-1:0]         rd_data_0;
wire                              bus_cmd_0;
wire[SYS_WIDTH-1:0]               bus_addr_0;
wire[CACHE_LINE_BITS-1:0]         bus_data_in_0;
wire                              bus_data_en_0;
wire                              bus_busy_0;

wire[SYS_WIDTH-1:0]               rd_addr_1;
wire                              rd_cmd_1;
wire[CACHE_LINE_BITS-1:0]         rd_data_1;
wire                              bus_cmd_1;
wire[SYS_WIDTH-1:0]               bus_addr_1;
wire[CACHE_LINE_BITS-1:0]         bus_data_in_1;
wire                              bus_data_en_1;
wire                              bus_busy_1; 

wire[SYS_WIDTH-1:0]               rd_addr_2;                                                                                            
wire                              rd_cmd_2;
wire[CACHE_LINE_BITS-1:0]         rd_data_2;
wire                              bus_cmd_2;
wire[SYS_WIDTH-1:0]               bus_addr_2;
wire[CACHE_LINE_BITS-1:0]         bus_data_in_2;
wire                              bus_data_en_2;
wire                              bus_busy_2; 

wire[SYS_WIDTH-1:0]               rd_addr_3;
wire                              rd_cmd_3;
wire[CACHE_LINE_BITS-1:0]         rd_data_3;
wire                              bus_cmd_3;
wire[SYS_WIDTH-1:0]               bus_addr_3;
wire[CACHE_LINE_BITS-1:0]         bus_data_in_3;
wire                              bus_data_en_3;
wire                              bus_busy_3; 

assign     rd_addr_0 = block_addr0;
assign     rd_cmd_0  = rd_en;
assign     rd_addr_1 = block_addr1;
assign     rd_cmd_1  = rd_en && filt_en; 
assign     rd_addr_2 = block_addr2;
assign     rd_cmd_2  = rd_en && filt_en; 
assign     rd_addr_3 = block_addr3;
assign     rd_cmd_3  = rd_en && filt_en; 

assign     force_en = {in_block_addr3[6],in_block_addr2[6],in_block_addr1[6],in_block_addr0[6]};


assign    l1_out_en = rd_data_en; 


assign    l1_dout0 = rd_data_0 & {(2**RAM_DATA_BITS){~in_block_addr_out0[6]}};
assign    l1_dout1 = rd_data_1 & {(2**RAM_DATA_BITS){~in_block_addr_out1[6]}};
assign    l1_dout2 = rd_data_2 & {(2**RAM_DATA_BITS){~in_block_addr_out2[6]}};
assign    l1_dout3 = rd_data_3 & {(2**RAM_DATA_BITS){~in_block_addr_out3[6]}};

assign    cache_busy = cache_busy_out || addr_fifo_first_full;  


quar_cache_top #(
    .SYS_WIDTH(SYS_WIDTH),  
    .BUS_WIDTH(BUS_WIDTH),
    .CACHE_SIZE(CACHE_SIZE),//(17),
    .CACHE_DATA_SIZE($clog2(CACHE_LINE_BITS))
) u_quar_cache_top
(
.SCAN_mode(SCAN_mode), 
.clk(clk),
.rst_n(arst_n),
.rec_busy(rec_busy),
.GPE_start(GPE_start),
.force_en(force_en),
.cache_busy_out(cache_busy_out),
.rd_data_en(rd_data_en),
.rd_addr_0(rd_addr_0),
.rd_cmd_0(rd_cmd_0),
.rd_data_0(rd_data_0),
.rd_addr_1(rd_addr_1),
.rd_cmd_1(rd_cmd_1),
.rd_data_1(rd_data_1),
.rd_addr_2(rd_addr_2),
.rd_cmd_2(rd_cmd_2),
.rd_data_2(rd_data_2),
.rd_addr_3(rd_addr_3),
.rd_cmd_3(rd_cmd_3),
.rd_data_3(rd_data_3),
.src(src[0:3])
);


assign inter_wr_rd = (!addr_fifo_first_empty) && (!addr_fifo_full); 

wire addr_first_full;

 distribute_fifo #( 
    .DWIDTH((2+7)*4),
    .DSIZE(4)
    )
    addr_fifo_first
(
//    .SCAN_mode(SCAN_mode), 
    .dataout(addr_fifo_inter_data), 
    .almost_full(addr_fifo_first_full), 
    .empty(addr_fifo_first_empty), 
    .datain({{block_addr3[4:3],in_block_addr3},{block_addr2[4:3],in_block_addr2},{block_addr1[4:3],in_block_addr1},{block_addr0[4:3],in_block_addr0}}),
    .wr(~addr_first_full && rd_en), 
    .clk(clk), 
    .rst_n(arst_n), 
    .rd(inter_wr_rd),
    .full(addr_first_full)
);   

 distribute_fifo #( 
    .DWIDTH((2+7)*4),
    .DSIZE(DATA_RD_FIFO_WIDTH)
    )
    addr_fifo
(
//    .SCAN_mode(SCAN_mode), 
    .dataout({{block_addr_out3,in_block_addr_out3},{block_addr_out2,in_block_addr_out2},{block_addr_out1,in_block_addr_out1},{block_addr_out0,in_block_addr_out0}}), 
    .full(addr_fifo_full), 
    .empty(), 
    .datain(addr_fifo_inter_data),
    .wr(inter_wr_rd), 
    .clk(clk), 
    .rst_n(arst_n), 
    .rd(l1_out_en)
);

assign fifo_full_error = (addr_fifo_full && addr_fifo_first_full ) && (~cache_busy && rd_en);

decoder #(
    .SYSTEM_ADDR_BITS(SYSTEM_ADDR_BITS),
    .MEMORY_DATA_WIDTH_LOG2(RAM_DATA_BITS)
) u_decoder_0 (
    .clk(clk),
    .rst_n(arst_n),
    .busy(rec_busy),
    .texture_block_addr(block_addr_out0), 
    .textype(textype), 
    .in_texture_addr(in_block_addr_out0), 
    .l1_dout(l1_dout0),
    .l1_out_en(l1_out_en),
    .color(color0), 
    .alpha(alpha0),
    .outen(decoder_outen0)
);

decoder #(
    .SYSTEM_ADDR_BITS(SYSTEM_ADDR_BITS),
    .MEMORY_DATA_WIDTH_LOG2(RAM_DATA_BITS)
) u_decoder_1 (
    .clk(clk),
    .rst_n(arst_n),
    .busy(rec_busy), 
    .texture_block_addr(block_addr_out1), 
    .textype(textype), 
    .in_texture_addr(in_block_addr_out1), 
    .l1_dout(l1_dout1),
    .l1_out_en(l1_out_en),
    .color(color1), 
    .alpha(alpha1),
    .outen(decoder_outen1)
);  

decoder #(
    .SYSTEM_ADDR_BITS(SYSTEM_ADDR_BITS),
    .MEMORY_DATA_WIDTH_LOG2(RAM_DATA_BITS)
) u_decoder_2 (
    .clk(clk),
    .rst_n(arst_n),
    .busy(rec_busy), 
    .texture_block_addr(block_addr_out2), 
    .textype(textype), 
    .in_texture_addr(in_block_addr_out2), 
    .l1_dout(l1_dout2),
    .l1_out_en(l1_out_en),
    .color(color2), 
    .alpha(alpha2),
    .outen(decoder_outen2)
);  

decoder #(
    .SYSTEM_ADDR_BITS(SYSTEM_ADDR_BITS),
    .MEMORY_DATA_WIDTH_LOG2(RAM_DATA_BITS)
) u_decoder_3 (
    .clk(clk),
    .rst_n(arst_n),
    .busy(rec_busy), 
    .texture_block_addr(block_addr_out3), 
    .textype(textype), 
    .in_texture_addr(in_block_addr_out3), 
    .l1_dout(l1_dout3),
    .l1_out_en(l1_out_en),
    .color(color3), 
    .alpha(alpha3),
    .outen(decoder_outen3)
);  

//***************************************
//for debug
//***************************************

reg[31:0]    rd_cmd_0_count;
reg[31:0]    rd_data_en_count; 
always@(posedge clk or negedge arst_n)
begin
    if(~arst_n)
        rd_cmd_0_count <= 0;
    else if(rd_cmd_0)
        rd_cmd_0_count <= rd_cmd_0_count + 1'b1;
end  

always@(posedge clk or negedge arst_n)
begin
    if(~arst_n)
        rd_data_en_count <= 0;
    else if(rd_data_en)
        rd_data_en_count <= rd_data_en_count + 1'b1;
end  


reg[31:0]     rd_en_count;
reg[31:0]     rd_back_en_count;
always@(posedge clk or negedge arst_n)
begin
    if(~arst_n)
        rd_en_count <= 0;
    else if(rd_en)
        rd_en_count <= rd_en_count + 1'b1;
end

always@(posedge clk or negedge arst_n)
begin
    if(~arst_n)
        rd_back_en_count <= 0;
    else if(l1_out_en)
        rd_back_en_count <= rd_back_en_count + 1'b1;
end  

always @(posedge clk)
begin
    if(fifo_full_error)
    begin
        $display("texture cache fifo full write.\n");
        #1000;
        $stop;
    end
end

integer  l1_out_fp0;
initial  l1_out_fp0=$fopen("l1_out_s.log","w");
integer  l1_out_fp1;
initial  l1_out_fp1=$fopen("l1_out_q.log","w");
always@(posedge clk)
  if(l1_out_en)
       $fwrite(l1_out_fp0,"%64h\n",l1_dout0);  

always@(posedge clk)
  if(l1_out_en)
       $fwrite(l1_out_fp1,"%64h\n%64h\n%64h\n%64h\n",l1_dout0,l1_dout1,l1_dout2,l1_dout3);


endmodule
