`include "gpu_bus.sv"
module quar_cache_top #(
            parameter  SYS_WIDTH = 32,
            parameter  BUS_WIDTH = 128,
            parameter  BL_WIDTH = 8,
            parameter  CACHE_SIZE = 15, // 2^15 bits, cache total size
            parameter  CACHE_DATA_SIZE = 7  // rd_data is 2^7 bits;   CACHE_DATA_SIZE  must  >  3
                       )
(
    input                              SCAN_mode,
    input                              clk,
    input                              rst_n,
    input                              rec_busy,
    input[3:0]                         force_en,
    output                             cache_busy_out,
    output                             rd_data_en,
    input                              GPE_start,
    input[SYS_WIDTH-1:0]               rd_addr_0,
    input                              rd_cmd_0,
    output[2**CACHE_DATA_SIZE-1:0]     rd_data_0,
    input[SYS_WIDTH-1:0]               rd_addr_1,
    input                              rd_cmd_1,
    output[2**CACHE_DATA_SIZE-1:0]     rd_data_1,
    input[SYS_WIDTH-1:0]               rd_addr_2,
    input                              rd_cmd_2,
    output[2**CACHE_DATA_SIZE-1:0]     rd_data_2,
    input[SYS_WIDTH-1:0]               rd_addr_3,
    input                              rd_cmd_3,
    output[2**CACHE_DATA_SIZE-1:0]     rd_data_3,
    gpu_bus                            src[4] 
);

localparam  bl = (2**CACHE_DATA_SIZE)/BUS_WIDTH-1;
localparam  QCACHE_FIFO_DEPTH = 2;

logic [2**CACHE_DATA_SIZE-1:0] fifo0_wdata;
logic [2**CACHE_DATA_SIZE-1:0] fifo1_wdata;
logic [2**CACHE_DATA_SIZE-1:0] fifo2_wdata;
logic [2**CACHE_DATA_SIZE-1:0] fifo3_wdata;
logic fifo0_wr;
logic fifo1_wr;
logic fifo2_wr;
logic fifo3_wr;
logic fifo0_empty;
logic fifo1_empty;
logic fifo2_empty;
logic fifo3_empty;
logic fifo0_full;
logic fifo1_full;
logic fifo2_full;
logic fifo3_full; 
logic rcmd_fifo0_afull;
logic rcmd_fifo1_afull;
logic rcmd_fifo2_afull;
logic rcmd_fifo3_afull;
logic rcmd_fifo0_empty;
logic rcmd_fifo1_empty;
logic rcmd_fifo2_empty;
logic rcmd_fifo3_empty;
logic [SYS_WIDTH-1:0] rcmd_fifo0_wdata;
logic [SYS_WIDTH-1:0] rcmd_fifo1_wdata;
logic [SYS_WIDTH-1:0] rcmd_fifo2_wdata;
logic [SYS_WIDTH-1:0] rcmd_fifo3_wdata;

fifo#(
    .DWIDTH(2**CACHE_DATA_SIZE),         
    .DSIZE(QCACHE_FIFO_DEPTH)
) rdata_fifo0
(
    .SCAN_mode(SCAN_mode),
    .datain(fifo0_wdata),
    .rd(rd_data_en),
    .wr(fifo0_wr),
    .rst_n(rst_n),
    .clk(clk), 
    .almost_full(),
    .dataout(rd_data_0), 
    .full(fifo0_full), 
    .empty(fifo0_empty)
);  


shifter#(
    .DATAIN_BIT($clog2(BUS_WIDTH)),
    .DATAOUT_BIT(CACHE_DATA_SIZE)
)u_sdram_rd_shifter0
(
    .clk(clk),
    .rst_n(rst_n),
    .datain(src[0].Grdata),
    .datain_en(src[0].Grvalid && src[0].Grready),
    .dataout(fifo0_wdata),
    .dataout_en(fifo0_wr)
);          






fifo#(
    .DWIDTH(2**CACHE_DATA_SIZE),         
    .DSIZE(QCACHE_FIFO_DEPTH)
) rdata_fifo1
(
    .SCAN_mode(SCAN_mode),
    .datain(fifo1_wdata),
    .rd(rd_data_en),
    .wr(fifo1_wr),
    .rst_n(rst_n),
    .clk(clk), 
    .almost_full(),
    .dataout(rd_data_1), 
    .full(fifo1_full), 
    .empty(fifo1_empty)
);       

shifter#(
    .DATAIN_BIT($clog2(BUS_WIDTH)),
    .DATAOUT_BIT(CACHE_DATA_SIZE)
)u_sdram_rd_shifter1
(
    .clk(clk),
    .rst_n(rst_n),
    .datain(src[1].Grdata),
    .datain_en(src[1].Grvalid && src[1].Grready),
    .dataout(fifo1_wdata),
    .dataout_en(fifo1_wr)
);          






fifo#(
    .DWIDTH(2**CACHE_DATA_SIZE),         
    .DSIZE(QCACHE_FIFO_DEPTH)
) rdata_fifo2
(
    .SCAN_mode(SCAN_mode),
    .datain(fifo2_wdata),
    .rd(rd_data_en),
    .wr(fifo2_wr),
    .rst_n(rst_n),
    .clk(clk), 
    .almost_full(),
    .dataout(rd_data_2), 
    .full(fifo2_full), 
    .empty(fifo2_empty)
);           

shifter#(
    .DATAIN_BIT($clog2(BUS_WIDTH)),
    .DATAOUT_BIT(CACHE_DATA_SIZE)
)u_sdram_rd_shifter2
(
    .clk(clk),
    .rst_n(rst_n),
    .datain(src[2].Grdata),
    .datain_en(src[2].Grvalid && src[2].Grready),
    .dataout(fifo2_wdata),
    .dataout_en(fifo2_wr)
);          






fifo#(
    .DWIDTH(2**CACHE_DATA_SIZE),         
    .DSIZE(QCACHE_FIFO_DEPTH)
) rdata_fifo3
(
    .SCAN_mode(SCAN_mode),
    .datain(fifo3_wdata),
    .rd(rd_data_en),
    .wr(fifo3_wr),
    .rst_n(rst_n),
    .clk(clk), 
    .almost_full(),
    .dataout(rd_data_3), 
    .full(fifo3_full), 
    .empty(fifo3_empty)
);    


shifter#(
    .DATAIN_BIT($clog2(BUS_WIDTH)),
    .DATAOUT_BIT(CACHE_DATA_SIZE)
)u_sdram_rd_shifter3
(
    .clk(clk),
    .rst_n(rst_n),
    .datain(src[3].Grdata),
    .datain_en(src[3].Grvalid && src[3].Grready),
    .dataout(fifo3_wdata),
    .dataout_en(fifo3_wr)
);          


fifo#(
    .DWIDTH(SYS_WIDTH),         
    .DSIZE(QCACHE_FIFO_DEPTH)
) rcmd_fifo0
(
    .SCAN_mode(SCAN_mode),
    .datain(rcmd_fifo0_wdata),
    .rd(src[0].Gvalid && src[0].Gready),
    .wr(rd_cmd_0),
    .rst_n(rst_n),
    .clk(clk), 
    .almost_full(rcmd_fifo0_afull),
    .dataout(src[0].Gaddr), 
    .full(), 
    .empty(rcmd_fifo0_empty)
);     

fifo#(
    .DWIDTH(SYS_WIDTH),         
    .DSIZE(QCACHE_FIFO_DEPTH)
) rcmd_fifo1
(
    .SCAN_mode(SCAN_mode),
    .datain(rcmd_fifo1_wdata),
    .rd(src[1].Gvalid && src[1].Gready),
    .wr(rd_cmd_1),
    .rst_n(rst_n),
    .clk(clk), 
    .almost_full(rcmd_fifo1_afull),
    .dataout(src[1].Gaddr), 
    .full(), 
    .empty(rcmd_fifo1_empty)
);     

fifo#(
    .DWIDTH(SYS_WIDTH),         
    .DSIZE(QCACHE_FIFO_DEPTH)
) rcmd_fifo2
(
    .SCAN_mode(SCAN_mode),
    .datain(rcmd_fifo2_wdata),
    .rd(src[2].Gvalid && src[2].Gready),
    .wr(rd_cmd_2),
    .rst_n(rst_n),
    .clk(clk), 
    .almost_full(rcmd_fifo2_afull),
    .dataout(src[2].Gaddr), 
    .full(), 
    .empty(rcmd_fifo2_empty)
);     

fifo#(
    .DWIDTH(SYS_WIDTH),         
    .DSIZE(QCACHE_FIFO_DEPTH)
) rcmd_fifo3
(
    .SCAN_mode(SCAN_mode),
    .datain(rcmd_fifo3_wdata),
    .rd(src[3].Gvalid && src[3].Gready),
    .wr(rd_cmd_3),
    .rst_n(rst_n),
    .clk(clk), 
    .almost_full(rcmd_fifo3_afull),
    .dataout(src[3].Gaddr), 
    .full(), 
    .empty(rcmd_fifo3_empty)
);     
        


assign cache_busy_out = (rcmd_fifo0_afull) ||
                        (rcmd_fifo1_afull) ||
                        (rcmd_fifo2_afull) ||
                        (rcmd_fifo3_afull) ;

assign rd_data_en = (!fifo0_empty) &&
                    (!fifo1_empty) &&
                    (!fifo2_empty) &&
                    (!fifo3_empty) &&
                    (!rec_busy)    ;

assign rcmd_fifo0_wdata = (rd_addr_0 >> (CACHE_DATA_SIZE-3)) << (CACHE_DATA_SIZE-3);
assign rcmd_fifo1_wdata = (rd_addr_1 >> (CACHE_DATA_SIZE-3)) << (CACHE_DATA_SIZE-3);
assign rcmd_fifo2_wdata = (rd_addr_2 >> (CACHE_DATA_SIZE-3)) << (CACHE_DATA_SIZE-3);
assign rcmd_fifo3_wdata = (rd_addr_3 >> (CACHE_DATA_SIZE-3)) << (CACHE_DATA_SIZE-3);


assign src[0].Gbl = bl;
assign src[1].Gbl = bl;
assign src[2].Gbl = bl;
assign src[3].Gbl = bl;

assign src[0].Gid = 'b0;
assign src[1].Gid = 'b0;
assign src[2].Gid = 'b0;
assign src[3].Gid = 'b0;

assign src[0].Gwrite = 1'b0;
assign src[1].Gwrite = 1'b0;
assign src[2].Gwrite = 1'b0;
assign src[3].Gwrite = 1'b0;

assign src[0].Gvalid = !rcmd_fifo0_empty;
assign src[1].Gvalid = !rcmd_fifo1_empty;
assign src[2].Gvalid = !rcmd_fifo2_empty;
assign src[3].Gvalid = !rcmd_fifo3_empty;

assign src[0].Gwdata = 'b0;
assign src[1].Gwdata = 'b0;
assign src[2].Gwdata = 'b0;
assign src[3].Gwdata = 'b0;

assign src[0].Gwvalid = 'b0;
assign src[1].Gwvalid = 'b0;
assign src[2].Gwvalid = 'b0;
assign src[3].Gwvalid = 'b0;

assign src[0].Gwfirst = 'b0;
assign src[1].Gwfirst = 'b0;
assign src[2].Gwfirst = 'b0;
assign src[3].Gwfirst = 'b0;
                              
assign src[0].Gwlast = 'b0;
assign src[1].Gwlast = 'b0;
assign src[2].Gwlast = 'b0;
assign src[3].Gwlast = 'b0;

assign src[0].Grready = !fifo0_full;
assign src[1].Grready = !fifo1_full;
assign src[2].Grready = !fifo2_full;
assign src[3].Grready = !fifo3_full;





endmodule

