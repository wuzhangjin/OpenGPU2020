
module alpha_decoder(
        input                   clk,
        input                   rst_n,
        input                   rd_en,
        input                   cache_busy,
    input       [63:0]      l1_dout,
    output  reg [7:0]       alpha,
        input       [3:0]       in_texture_addr
);

reg  [7:0]    pixel_tag;


//-----------------------------
//|Word address | 8-bit Alpha  |
//+-------------+--------------+
//|    0        |  Alpha0      |
//|    1        |  Alpha1      |
//|    2        |  Alpha2      |
//|    3        |  Alpha3      |
//|    4        |  Alpha4      |     
//|    5        |  Alpha5      |    
//|    6        |  Alpha6      |        
//|    7        |  Alpha4      |    
//-----------------------------

//for l1_dout
//-----------------------
//|Bits   |     Texel   |
//+-------+-------------+
//|7:0    | Alpha0      |
//|15:8   | Alpha1      |
//|63:16  | 16*3 index  |
//-----------------------


//for Bitmap_word(l1_dout[63:16])
//-----------------------
//|Bits   |     Texel   |
//+-------+-------------+
//|2:0    | Alpha[0][0] |
//|5:3    | Alpha[0][1] |
//|8:6    | Alpha[0][2] |
//|11:9   | Alpha[0][3] |
//|9:8    | Alpha[1][0] |
//|11:10  | Alpha[1][1] |
//|13:12  | Alpha[1][2] |
//|15:14  | Alpha[1][3] |
//-----------------------

//Code Encoding for transparcy
//if alpha0 > alpha1
//000:   alpha0
//001:   alpha1
//010:   6/7 color_0 + 1/7 color_1
//011:   5/7 color_0 + 2/7 color_1
//100:   4/7 color_0 + 3/7 color_1
//101:   3/7 color_0 + 4/7 color_1
//110:   2/7 color_0 + 5/7 color_1
//111:   1/7 color_0 + 6/7 color_1
//else 
//


wire  [63:0]   texel = l1_dout;

wire  [7:0]   base_alpha0 = texel[7:0];
wire  [7:0]   base_alpha1 = texel[15:8];

reg [7:0] base_alpha0_ff1;
reg [7:0] base_alpha1_ff1;
reg mask_ff1;


wire          transprancy;

reg cache_busy_ff1;
reg cache_busy_ff2;

reg  [11:0]   alpha_choose_inter;
reg  [2:0]    alpha_choose;
reg [2:0] alpha_choose_ff1;
reg  [2:0]    reg_alpha_choose;
reg           reg_mask;


reg  [2:0]    reg_alpha_choose_before;
reg           reg_mask_before;


wire    mask = (base_alpha0 > base_alpha1) ? 1'b1: 1'b0;  //1: disbale transprancy, which means mask.
assign    transprancy = ~mask;


reg     rd_en_ff1;
reg     rd_en_ff2;
reg     rd_en_ff3;
reg     rd_en_ff4;

always @(posedge clk or negedge rst_n)
begin
    if(!rst_n)
        begin
                rd_en_ff1 <=  0;
                rd_en_ff2 <=  0;
                rd_en_ff3 <=  0;
                rd_en_ff4 <=  0;            
        end
        else
        begin
                rd_en_ff1 <=  rd_en;
                rd_en_ff2 <=  rd_en_ff1;
                rd_en_ff3 <=  rd_en_ff2;
                rd_en_ff4 <=  rd_en_ff3;                            
        end
end


//stage 1


//48 choose 4 * 12
always@(*)
begin
        case(in_texture_addr[3:2])
                2'b00: alpha_choose_inter = l1_dout[27:16];
                2'b01: alpha_choose_inter = l1_dout[39:28];
                2'b10: alpha_choose_inter = l1_dout[51:40];
                2'b11: alpha_choose_inter = l1_dout[63:52];
        endcase
end

//4 choose 1
always@(*)
begin
        case(in_texture_addr[1:0])
                2'b00: alpha_choose = alpha_choose_inter[2:0];
                2'b01: alpha_choose = alpha_choose_inter[5:3];
                2'b10: alpha_choose = alpha_choose_inter[8:6];
                2'b11: alpha_choose = alpha_choose_inter[11:9];
        endcase
end

wire [7:0]    alpha2_0 = (mask == 1) ? 219: 205;
wire [7:0]    alpha3_0 = (mask == 1) ? 183: 154;
wire [7:0]    alpha4_0 = (mask == 1) ? 146: 102;
wire [7:0]    alpha5_0 = (mask == 1) ? 110: 51 ;
wire [7:0]    alpha6_0 = (mask == 1) ? 73 : 0  ;
wire [7:0]    alpha7_0 = (mask == 1) ? 37 : 0  ;
wire [7:0]    alpha2_1 = (mask == 1) ?  37:  51;
wire [7:0]    alpha3_1 = (mask == 1) ?  73: 102;
wire [7:0]    alpha4_1 = (mask == 1) ? 110: 154;
wire [7:0]    alpha5_1 = (mask == 1) ? 146: 205;
wire [7:0]    alpha6_1 = (mask == 1) ? 183: 0  ;
wire [7:0]    alpha7_1 = (mask == 1) ? 219: 0  ;
reg  [7:0]    alpha0_in;
reg  [7:0]    alpha1_in;
reg  [7:0]    alpha_out;
reg  [7:0]    alpha0;
reg  [7:0]    alpha1;
reg  [7:0]    alpha0_before;
reg  [7:0]    alpha1_before;
reg [15:0] temp1;
reg [15:0] temp2;

always @(posedge clk or negedge rst_n)
begin
        if(~rst_n)
                alpha0_in <=  0;
        else if(rd_en)
                begin
                        case(alpha_choose)
                                3'b000: alpha0_in <=  0;
                                3'b001: alpha0_in <=  0;
                                3'b010: alpha0_in <=  alpha2_0;
                                3'b011: alpha0_in <=  alpha3_0;
                                3'b100: alpha0_in <=  alpha4_0;
                                3'b101: alpha0_in <=  alpha5_0;
                                3'b110: alpha0_in <=  alpha6_0;
                                3'b111: alpha0_in <=  alpha7_0;
                        endcase
                end
end

always @(posedge clk or negedge rst_n)
begin
        if(~rst_n)
                alpha1_in <=  0;
        else if(rd_en)
                begin
                        case(alpha_choose)
                                3'b000: alpha1_in <=  0;
                                3'b001: alpha1_in <=  0;
                                3'b010: alpha1_in <=  alpha2_1;
                                3'b011: alpha1_in <=  alpha3_1;
                                3'b100: alpha1_in <=  alpha4_1;
                                3'b101: alpha1_in <=  alpha5_1;
                                3'b110: alpha1_in <=  alpha6_1;
                                3'b111: alpha1_in <=  alpha7_1;
                        endcase
                end
end


always@(posedge clk or negedge rst_n)
begin
        if(~rst_n)
                begin
                        base_alpha0_ff1 <=  0;
                        base_alpha1_ff1 <=  0;
                        mask_ff1 <=  0;
                        alpha_choose_ff1 <=  0;
                end
        else if(rd_en)
                begin
                        base_alpha0_ff1 <=  base_alpha0;
                        base_alpha1_ff1 <=  base_alpha1;    
                        mask_ff1 <=  mask;
                        alpha_choose_ff1 <=  alpha_choose;
                end
end


//stage 2


always @(posedge clk or negedge rst_n)
begin
    if(!rst_n)
            temp1 <=  0;
        else if(rd_en_ff1)
            temp1 <=  alpha0_in * base_alpha0_ff1;
end

always @(posedge clk or negedge rst_n)
begin
    if(!rst_n)
            temp2 <=  0;
        else if(rd_en_ff1)
            temp2 <=  alpha1_in * base_alpha1_ff1;
end





always @(posedge clk or negedge rst_n)
begin
   if(!rst_n)
        begin
            alpha0 <=  0;
                alpha1 <=  0;
                reg_mask <=  0;
                reg_alpha_choose <=  0;
        end
        else if(rd_en_ff1)
        begin
                alpha0 <=  base_alpha0_ff1;
                alpha1 <=  base_alpha1_ff1;
                reg_mask <=  mask_ff1;
                reg_alpha_choose <=  alpha_choose_ff1;
        end
end

//mab_alpha uut_mab_alpha(alpha0_in, alpha1_in, base_alpha0, base_alpha1, alpha_out);
always @(*)
begin
    alpha_out = (temp1 + temp2) >> 8;
end

// stage 3

always@(posedge clk or negedge rst_n)
begin
        if(~rst_n) alpha <=  0;
        else if(rd_en_ff2)
        begin
                case(reg_alpha_choose)
                        3'b000: alpha <=  alpha0;
                        3'b001: alpha <=  alpha1;
                        3'b010: alpha <=  alpha_out;
                        3'b011: alpha <=  alpha_out;
                        3'b100: alpha <=  alpha_out;
                        3'b101: alpha <=  alpha_out;
                        3'b110: alpha <=  (reg_mask == 1) ? alpha_out : 8'd0   ;
                        3'b111: alpha <=  (reg_mask == 1) ? alpha_out : 8'd255 ;
                endcase
        end
end




endmodule
