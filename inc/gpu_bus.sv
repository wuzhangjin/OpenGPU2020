
//-----------------------------------------------------------------------------
//
//Copyright(c) 2020, ThorsianWay Technologies Co, Ltd
//All rights reserved.
//
//IP Name       :   gpu_bus
//File Name     :   gpu_bus.v
//Module name   :   gpu_bus interface
//Full name     :   OpenGpu 2020 internal Bus
//
//Author        :   xiang tian
//Email         :   
//Data          :   2020/5/5
//Version       :   V1.0
//
//Abstract      :   
//                  
//Called  by    :   GPU
//
//Modification history
//-----------------------------------------------------
//1.00: intial version 
//
//
//-----------------------------------------------------------------------------

//-----------------------------
//DEFINE MACRO
//-----------------------------      
`ifndef GPU_BUS_DEF
`define GPU_BUS_DEF

interface gpu_bus
#(
    parameter ADDR_WIDTH=32,
    parameter DATA_WIDTH=128,
    parameter ID_WIDTH=4,
    parameter BL_WIDTH=8
 );

// addr interface
logic [ADDR_WIDTH-1:0] Gaddr;
logic [BL_WIDTH-1:0]   Gbl;
logic [ID_WIDTH-1:0]   Gid;
logic                  Gwrite;
logic                  Gvalid;
logic                  Gready;

//write data interface
logic [DATA_WIDTH-1:0] Gwdata;
logic                  Gwvalid;
logic                  Gwready;
logic                  Gwfirst;
logic                  Gwlast;

//read data interface
logic [DATA_WIDTH-1:0] Grdata;
logic [ID_WIDTH-1:0]   Grid;
logic                  Grvalid;
logic                  Grready;
logic                  Grfirst;
logic                  Grlast;

modport source(
    output Gaddr,
    output Gbl,
    output Gid,
    output Gwrite,
    output Gvalid,
    output Gwdata,
    output Gwvalid,
    output Gwfirst,
    output Gwlast,
    output Grready, 
    input Gready,
    input Gwready,
    input Grdata,
    input Grid,
    input Grvalid,
    input Grfirst,
    input Grlast
);


modport sink(
    input Gaddr,
    input Gbl,
    input Gid,
    input Gwrite,
    input Gvalid,
    input Gwdata,
    input Gwvalid,
    input Gwfirst,
    input Gwlast,
    input Grready, 
    output Gready,
    output Gwready,
    output Grdata,
    output Grid,
    output Grvalid,
    output Grfirst,
    output Grlast
);


endinterface

`endif
